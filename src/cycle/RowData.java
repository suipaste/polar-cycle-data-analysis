/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cycle;

/**
 *
 * @author Stephen
 */
public class RowData {
    // Fields
    String time;
    String heartRate;
    String speed;
    String cadence;
    String altitude;
    String power;
    String lrBalance;
    
    public RowData(String initialTime, String initialHeartRate, String initialSpeed, String initialCadence, String initialAltitude, String initialPower, String initialLrBalance) {
    time = initialTime;
    heartRate = initialHeartRate;
    speed = initialSpeed;
    cadence = initialCadence;
    altitude = initialAltitude;
    power = initialPower;
    lrBalance = initialLrBalance;
    }
}
